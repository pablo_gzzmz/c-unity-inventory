
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory Profile", menuName = "Profiles/Inventory")]
public class InventoryProfile : ScriptableObject {

    [SerializeField]
    private BagData[] bagData;
    public  BagData[] BagData { get { return bagData; } }


}
