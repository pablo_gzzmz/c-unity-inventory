
using UnityEngine;
using System.Collections.Generic;

public class Tester : MonoBehaviour {

    [SerializeField]
    private Transform canvas;

    private Inventory inventory;

    private List<ItemData> data = new List<ItemData>();

    private void Awake() {
        Database.Initialize();
        UIHelper.Initialize();

        SetInventory();

        Instantiate(Resources.Load("UI/Inventory"), canvas);
        UIInventory.Instance.SetInventory(inventory);

        // Item add testing
        string name = "Item1";
        int hash = name.GetHashCode();
        data.Add(Database.GetItem(hash));

        name = "Item2";
        hash = name.GetHashCode();
        data.Add(Database.GetItem(hash));
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.K)) {
            Item item = new Item(data[0]);

            inventory.TryAddElement(item);
        }

        if (Input.GetKeyDown(KeyCode.L)) {
            Item item = new Item(data[1]);

            inventory.TryAddElement(item);
        }
    }

    private void SetInventory() {
        BagData[] data = Resources.Load<InventoryProfile>("Profiles/Inventory Profile").BagData;
        Inventory.Bag[] bags = new Inventory.Bag[Globals.bagMax];

        for (int i = 0; i < bags.Length; i++) {
            bags[i] = new Inventory.Bag();
            bags[i].data = data[i];
            bags[i].empty = false;
        }

        inventory = new Inventory(bags);
    }
}
