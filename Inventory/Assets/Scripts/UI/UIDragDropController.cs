
using System;
using UnityEngine;

public class UIDragDropController : Singleton<UIDragDropController> {

    public enum States {
        None,
        Checking,
        Dragging
    }

    private static States state;
    public  static States State { get { return state; } }

    private static UISlot   originSlot;
    private static IElement draggedContent;
    public  static IElement DraggedContent { get { return draggedContent; } }
    private static Vector2  mouseDragStart;
    private static Vector2  mouse;
    private static float    clickZone;

    public static event Action OnDragStarted;
    public static event Action OnDragEnded;
    
    private void Update() {
        mouse = Input.mousePosition;

        switch (state) {
            case States.Checking:
                if (DetectDragging(mouseDragStart, mouse)) StartDrag();
                break;
        }
    }

    public static void PointerDown(UISlot slot) {
        switch (state) {
            case States.None:
                if (!slot.HasContent) return;

                state = States.Checking;
                clickZone = slot.Icon.rectTransform.sizeDelta.x * 0.4f;
                mouseDragStart = Input.mousePosition;
                originSlot = slot;
                break;
        }
    }

    public static void PointerUp(UISlot slot) {
        switch (state) {
            case States.Dragging:
                if (slot == originSlot) {
                    Cancel();
                    return;
                }

                EndDrag(slot);
                break;

            case States.Checking:
                state = States.None;
                break;
        }
    }

    private static void StartDrag() {
        state = States.Dragging;

        draggedContent = originSlot.Content;
        SetColorDrag();

        if (OnDragStarted != null) OnDragStarted.Invoke();
    }

    private static void EndDrag(UISlot slot) {
        SetColorOriginal();

        // Swap
        if (slot.HasContent) {
            originSlot.SetContent(slot.Content);
        }
        else {
            originSlot.SetEmpty();
        }

        slot.SetContent(draggedContent);
        state = States.None;

        if (OnDragEnded != null) OnDragEnded.Invoke();
    }

    public static void Cancel() {
        if (state != States.None) {
            state = States.None;
            SetColorOriginal();

            if (OnDragEnded != null) OnDragEnded.Invoke();
        }
    }

    private static void SetColorOriginal() {
        Color color = originSlot.Icon.color;
        color.a = 1;
        originSlot.Icon.color = color;
        color = originSlot.Outline.color;
        color.a = 1;
        originSlot.Outline.color = color;
    }

    private static void SetColorDrag() {
        Color color = originSlot.Icon.color;
        color.a = 0.75f;
        originSlot.Icon.color = color;
        color = originSlot.Outline.color;
        color.a = 0.0f;
        originSlot.Outline.color = color;
    }

    private static bool DetectDragging(Vector2 dragStartPoint, Vector2 newPoint) {
        if ((newPoint.x > dragStartPoint.x + clickZone || newPoint.x < dragStartPoint.x - clickZone) ||
            (newPoint.y > dragStartPoint.y + clickZone || newPoint.y < dragStartPoint.y - clickZone))
            return true;
        else
            return false;
    }
}
