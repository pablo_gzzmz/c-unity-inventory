
using UnityEngine;

public class UIHelper {

    private static GameObject slot;
    public  static GameObject Slot { get { return slot; } }

    private static GameObject bag;
    public  static GameObject Bag { get { return bag; } }

    private static GameObject icon;
    public  static GameObject Icon { get { return icon; } }

    public static void Initialize() {
        slot  = Resources.Load<GameObject>("UI/Slot");
        bag   = Resources.Load<GameObject>("UI/Bag");
        icon  = Resources.Load<GameObject>("UI/Icon");
    }
}
