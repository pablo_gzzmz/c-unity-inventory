
using UnityEngine;

public class UIInventory : Singleton<UIInventory> {
    
    private Inventory inventory;

    private Transform[] pivots;
    private UIBag[] bags;

    public void SetInventory(Inventory _inventory) {
        inventory = _inventory;

        pivots = new Transform[Globals.bagMax];

        pivots[0] = transform.GetChild(0);
        for (int i = 0; i < inventory.Bags.Length; i++) {
            if (i > 0) pivots[i] = Instantiate(pivots[0], transform).transform;
        }

        bags = new UIBag[inventory.Bags.Length];
        for (int i = 0; i < inventory.Bags.Length; i++) {
            SetUpBag(i, inventory.Bags[i]);
        }

        inventory.OnDataChange   += SetUpBag;
        inventory.OnElementAdded += LinearAdd;
    }

    private void OnDestroy() {
        RemoveActions();
    }

    private void SetUpBag(int index, Inventory.Bag bag) {
        if (bags[index] != null) Destroy(bags[index].gameObject);

        if (bag.empty) return;

        GameObject go = Instantiate(UIHelper.Bag, pivots[index]);
        go.transform.SetSiblingIndex(index);
        bags[index] = go.GetComponent<UIBag>();
        bags[index].Setup(index, bag.data.Slots, inventory);
    }

    private void LinearAdd(IElement element) {
        for (int i = 0; i < bags.Length; i++) {
            if (bags[i].IsFull) continue;
            
            bags[i].LinearAdd(element);
            return;
        }

        Debug.Assert(false, "Incoherent sizes between total bag slots and inventory size");
    }

    private void RemoveActions() {
        if (inventory != null) {
            inventory.OnDataChange   -= SetUpBag;
            inventory.OnElementAdded -= LinearAdd;
        }
    }
}