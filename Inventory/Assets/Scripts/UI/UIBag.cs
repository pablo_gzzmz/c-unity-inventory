
using UnityEngine;
using UnityEngine.UI;

public class UIBag : UIMaskedOpenCloser {
    
    [SerializeField]
    private GridLayoutGroup buttonGroup;
    public  GridLayoutGroup ButtonGroup { get { return buttonGroup; } }
    
    private UISlot[] slots;
    public  UISlot[] Slots { get { return slots; } }

    private int count;
    public  int Count { get { return count; } }

    private bool isFull;
    public  bool IsFull { get { return isFull; } }

    public void Setup(int bagIndex, int capacity, ElementContainer container) {
        slots = new UISlot[capacity];

        for (int i = 0; i < slots.Length; i++) {
            GameObject go = Instantiate(UIHelper.Slot, buttonGroup.transform);
            slots[i] = go.GetComponent<UISlot>();
            slots[i].CDImage.enabled = false;
            slots[i].TopText.enabled = false;
            slots[i].SetSize(Globals.bagSlotSize);
            slots[i].SetContainer(container);

            int index = i;
            slots[i].OnRightUp += () => { AttemptUseSlot(index); };

            slots[i].OnContent += OnSlotContent;
            slots[i].OnEmpty   += OnSlotEmpty;
        }
        
        ForceHeight(GetBagHeight());
        Close();
    }

    public void LinearAdd(IElement element) {
        for (int i = 0; i < slots.Length; i++) {
            if (slots[i].HasContent) continue;

            slots[i].SetContent(element);
            return;
        }
    }

    public void Switch() {
        if (isOpen) Close();
        else OpenBag();
    }

    public void OpenBag() {
        Open(GetBagHeight());
    }

    private float GetBagHeight() {
        float heightResult;
        heightResult  = buttonGroup.cellSize.y + buttonGroup.spacing.y;
        heightResult *= (slots.Length+1) / buttonGroup.constraintCount;
        heightResult += buttonGroup.padding.bottom;
        heightResult -= 1; // If icon has outline
        return heightResult;
    }

    private void AttemptUseSlot(int index) {
        if(slots[index].HasContent)
            slots[index].Content.Use();
    }

    private void OnSlotContent() {
        count++;
        if (count == slots.Length) isFull = true;
    }

    private void OnSlotEmpty() {
        count--;
        isFull = false;
    }
}
