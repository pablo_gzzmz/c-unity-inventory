
using System;
using UnityEngine;

/// <summary>
/// Component used to allow a masked rect containing buttons to be opened and closed smoothly via damp function.
/// </summary>

[Serializable]
public class UIMaskedOpenCloser : MonoBehaviour {

    [SerializeField]
    private float openCloseTime = 0.1f;

    [SerializeField]
    protected RectTransform  opener; // IElement to move up and down
    protected Vector2 startPosition;
    protected Vector2 startSize;

    [SerializeField]
    protected RectTransform  maskTransform; // Transform to control the mask for the button slots

    private float currentHeight;
    private float targetHeight = 0;
    private float heightVelocity;
    private Vector2 position;
    private Vector2 size;

    protected bool isOpen;
    public    bool IsOpen { get { return isOpen; } }

    protected virtual void Awake() {
        startPosition = opener.anchoredPosition;
        position = startPosition;
        startSize = maskTransform.sizeDelta;
        size = startSize;
    }

    protected virtual void Update() {
        // Move
        currentHeight = Mathf.SmoothDamp(currentHeight, targetHeight, ref heightVelocity, openCloseTime, 5000, Time.unscaledDeltaTime);
        position.y = startPosition.y + currentHeight;
        opener.anchoredPosition = position;

        // Mask size
        size.y = currentHeight;
        maskTransform.sizeDelta = size;
    }

    public void Open(float height) {
        isOpen = true;
        SetHeight(height);
    }

    public void Close() {
        isOpen = false;
        SetHeight(0);
    }

    protected void SetHeight(float height) {
        targetHeight = height;
        heightVelocity = 0;
    }

    protected void ForceHeight(float height) {
        SetHeight(height);
        currentHeight = height;
        position.y = startPosition.y + height;
        opener.anchoredPosition = position;

        // Mask size
        size.y = height;
        maskTransform.sizeDelta = size;
    }
}
