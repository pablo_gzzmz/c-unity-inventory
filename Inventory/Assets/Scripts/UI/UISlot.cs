
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

/// <summary>
/// UI Slots are components that portray individual contents of an element container.
/// </summary>

[RequireComponent(typeof(RectTransform))]
public class UISlot : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler {
   
    [SerializeField]
    [HideInInspector]
    private RectTransform rectTransform;

    [SerializeField]
    private Image icon;
    public  Image Icon { get { return icon; } }

    [SerializeField]
    private Image outline;
    public  Image Outline { get { return outline; } }

    [SerializeField]
    private Image cdImage;
    public  Image CDImage { get { return cdImage; } }

    [SerializeField]
    private Text topText;
    public  Text TopText { get { return topText; } }
    
    [SerializeField]
    private Text bottomText;
    public  Text BottomText { get { return bottomText; } }

    private IElement content;
    public  IElement Content { get { return content; } set { content = value; } }

    private ElementContainer container;
    public  ElementContainer Container { get { return container; } }

    private bool hasContent;
    public  bool HasContent { get { return hasContent; } }

    private bool contentExistsInSlot;
    public  bool ContentExistsInSlot { get { return contentExistsInSlot; } }

    private bool hovered;

    public event Action OnLeftDown;
    public event Action OnLeftUp;
    public event Action OnRightUp;
    public event Action OnContent;
    public event Action OnEmpty;

    private void OnValidate() {
        rectTransform = GetComponent<RectTransform>();
        SetSize(rectTransform.sizeDelta);
    }

    private void OnRectTransformDimensionsChange() {
        SetChildrenSize();
    }

    private void Awake() {
        topText.enabled = false;

        hasContent = true;
        SetEmpty();
    }

    private void Update() {
        if (hasContent) cdImage.fillAmount = content.CooldownFill;
    }

    public bool SetContent(IElement _content) {
        Debug.Assert(container.Elements.ContainsKey(_content.NameID) || container.Elements.ContainsKey(_content.UniqueID));

        if ((_content.Type & container.Restriction) != 0) return false;

        bool wasEmpty = !hasContent;
        if (content != null && content.CanStack) {
            content.OnStackChange -= SetStacks;
        }

        content = _content;
        hasContent = true;

        icon.sprite = content.Icon;
        outline.enabled    = true;
        BottomText.enabled = true;
        outline.color = content.Color;

        if (content.CanStack) {
            SetStacks(content.Stacks);
            content.OnStackChange += SetStacks;
        }
        else bottomText.enabled = false;
        
        if (wasEmpty && OnContent != null) OnContent.Invoke();

        return true;
    }

    public void SetEmpty() {
        if (!hasContent) return;

        if (content != null) {
            content.OnStackChange -= SetStacks;
            content = null;
        }

        hasContent = false;
        icon.sprite = GenericAssets.EmptyIcon;
        outline.enabled = false;
        bottomText.enabled = false;
        cdImage.fillAmount = 0;

        if (OnEmpty != null) OnEmpty.Invoke();
    }
    
    public void SetSize(Vector2 size) {
        rectTransform.sizeDelta = size;
        SetChildrenSize();
    }

    private void SetStacks(int stacks) {
        if (stacks > 1) {
            bottomText.enabled = true;
            BottomText.text = stacks.ToString();
        }
        else bottomText.enabled = false;
    }

    private void SetChildrenSize() {
        Vector2 size = rectTransform.sizeDelta;
        icon.rectTransform.sizeDelta = size;
        size.x += 2;
        size.y += 2;
        outline.rectTransform.sizeDelta = size;
    }

    public void SetContainer(ElementContainer _container, bool _contentExistsInSlot = true) {
        container = _container;
        contentExistsInSlot = _contentExistsInSlot;
    }
    
    public void OnPointerDown(PointerEventData eventData) {
        switch (eventData.button) {
            case PointerEventData.InputButton.Left:
                if (OnLeftDown != null) OnLeftDown.Invoke();
                UIDragDropController.PointerDown(this);
                break;
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (!hovered) return;

        switch (eventData.button) {
            case PointerEventData.InputButton.Left:
                if (OnLeftUp != null) OnLeftUp.Invoke();
                UIDragDropController.PointerUp(this);
                break;

            case PointerEventData.InputButton.Right:
                if (OnRightUp != null) OnRightUp.Invoke();
                break;
        }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        hovered = true;
    }

    public void OnPointerExit(PointerEventData eventData) {
        hovered = false;
    }
}
