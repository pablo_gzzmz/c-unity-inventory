
using System;
using System.Collections.Generic;
using UnityEngine;

public enum CursorSprites {
    Default,
    UIDragDrop
}

public class CursorController : Singleton<CursorController> {

    private static Dictionary<CursorSprites, Texture2D> cursorSprites;
    private static readonly Vector2 hotspot = new Vector2(0, 2);

    public static event Action OnCursorHidden;
    public static event Action OnCursorVisible;

    private static event Action OnUIDragDropStarted;
    private static event Action OnUIDragDropEnded;

    protected override void SingletonAwake() {
        cursorSprites = new Dictionary<CursorSprites, Texture2D>();
        foreach (CursorSprites sprites in Enum.GetValues(typeof(CursorSprites))) {
            if (sprites == CursorSprites.UIDragDrop) continue;

            cursorSprites.Add(sprites, Resources.Load<Texture2D>("UI/Cursors/" + sprites.ToString()));
        }

        SetCursorSprite(CursorSprites.Default);

        OnUIDragDropStarted = () => { SetCursorSprite(CursorSprites.UIDragDrop); };
        OnUIDragDropEnded = () => { SetCursorSprite(CursorSprites.Default); };

        UIDragDropController.OnDragStarted += OnUIDragDropStarted;
        UIDragDropController.OnDragEnded += OnUIDragDropEnded;
    }

    protected override void SingletonOnDestroy() {
        UIDragDropController.OnDragStarted += OnUIDragDropStarted;
        UIDragDropController.OnDragEnded += OnUIDragDropEnded;
    }

    private static void SetCursorSprite(CursorSprites type) {
        switch (type) {
            case CursorSprites.UIDragDrop:
                Cursor.SetCursor(UIDragDropController.DraggedContent.CursorIcon, hotspot, CursorMode.Auto);
                break;

            default:
                Cursor.SetCursor(cursorSprites[type], hotspot, CursorMode.Auto);
                break;
        }
    }
}
