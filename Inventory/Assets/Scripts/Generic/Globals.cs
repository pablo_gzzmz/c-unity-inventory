
using UnityEngine;
using System.Collections.Generic;

public static class Globals {
    
    // Itemization variables
    public const ElementMask inventoryRestriction = ElementMask.Item;
    public const int bagMax = 3;
    public const int bagSlotMax = 30;

    public static readonly Color emptyColor = new Color(0, 0, 0, 0);
    
    public static readonly Vector2 bagSlotSize = new Vector2(42, 42);
    public static readonly Vector3 offsightUI  = new Vector3(-99999, -99999, 0);

    public static readonly Dictionary<ItemRarity, Color> itemRarityColors = new Dictionary<ItemRarity, Color>() {
        { ItemRarity.None,      new Color(0.00f, 0.00f, 0.00f) },
        { ItemRarity.Poor,      new Color(0.50f, 0.50f, 0.50f) },
        { ItemRarity.Common,    new Color(1.00f, 1.00f, 1.00f) },
        { ItemRarity.Uncommon,  new Color(0.20f, 1.00f, 0.00f) },
        { ItemRarity.Rare,      new Color(0.00f, 0.45f, 0.85f) },
        { ItemRarity.Epic,      new Color(0.60f, 0.15f, 0.85f) },
        { ItemRarity.Legendary, new Color(1.00f, 0.50f, 0.00f) }
    };

    public static readonly Dictionary<LogMessageType, Color> logMessageColors = new Dictionary<LogMessageType, Color>() {
        { LogMessageType.Error, new Color(1f, 0.25f, 0) },
        { LogMessageType.Info,  new Color(1f, 1f, 1f) }
    };
}
