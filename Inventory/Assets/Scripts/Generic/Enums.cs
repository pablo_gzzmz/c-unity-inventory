
using System;

[Flags]
public enum ElementMask {
    Item,
    Ability
}

public enum ItemRarity {
    None,
    Poor,
    Common,
    Uncommon,
    Rare,
    Epic,
    Legendary
}

public enum LogMessageType {
    Error,
    Info
}