
using System;
using System.Collections.Generic;

public class ElementContainer {

    protected Dictionary<long, IElement> elements;
    public    Dictionary<long, IElement> Elements { get { return elements; } }

    protected ElementMask restriction;
    public    ElementMask Restriction { get { return restriction; } }

    protected int capacity;
    public    int Capacity { get { return capacity; } }

    protected bool isFull;
    public    bool IsFull { get { return isFull; } }

    public event Action<IElement> OnElementAdded;
    public event Action<IElement> OnElementRemoved;

    public ElementContainer(int initialCapacity, int dictionaryCapacity, ElementMask _restriction) {
        capacity = initialCapacity;
        restriction = _restriction;

        elements = new Dictionary<long, IElement>(dictionaryCapacity);
    }

    public bool TryChangeCapacity(int newCapacity) {
        if (elements.Count > newCapacity) return false;

        capacity = newCapacity;
        return true;
    }

    public bool TryAddElement(IElement element) {
        if (isFull && !element.CanStack || (element.Type & restriction) != 0) return false;

        if (element.CanStack) {
            IElement stack;

            if (elements.TryGetValue(element.NameID, out stack)) {
                stack.Stacks += element.Stacks;
                return true;
            }
            else {
                if (isFull) return false;

                elements.Add(element.NameID, element);
            }
        }
        else 
            elements.Add(element.UniqueID, element);
        
        if (elements.Count == capacity) isFull = true;
        if (OnElementAdded != null) OnElementAdded.Invoke(element);
        return true;
    }

    public bool TryRemoveElement(IElement _element) {
        IElement element;

        if (_element.CanStack) {
            if (elements.TryGetValue(_element.NameID, out element)) {
                if (element.Stacks > 1) {
                    element.Stacks--;
                    UnityEngine.Debug.Log(element.Stacks);
                    return true;
                }
            }

            elements.Remove(element.NameID);
            if (OnElementRemoved != null) OnElementRemoved.Invoke(element);
            isFull = false;
            return true;
        }

        else {
            if (elements.TryGetValue(_element.UniqueID, out element)) {
                elements.Remove(element.UniqueID);
                if (OnElementRemoved != null) OnElementRemoved.Invoke(element);
                isFull = false;
                return true;
            }
        }
        return false;
    }
}
