
using UnityEngine;
using System;

public class Inventory : ElementContainer {

    public class Bag {
        public BagData data;
        public bool    empty = true;
    }

    private Bag[] bags = new Bag[Globals.bagMax];
    public  Bag[] Bags { get { return bags; } }

    public event Action<int, Bag> OnDataChange;

    public Inventory(Bag[] startBags) : base(0, Globals.bagMax * Globals.bagSlotMax, Globals.inventoryRestriction) {
        for (int i = 0; i < bags.Length; i++) {
            bags[i] = new Bag();

            TryUpdateData(i, startBags[i]);
        }
    }

    public bool TryUpdateData(int index, Bag newBag) {
        int resultingCapacity = capacity;

        if (newBag.empty && !bags[index].empty) {
            resultingCapacity -= bags[index].data.Slots;
        }

        else if (!bags[index].empty) {
            resultingCapacity -= bags[index].data.Slots;
            resultingCapacity += newBag.data.Slots;
        }
        else {
            resultingCapacity += newBag.data.Slots;
        }

        if (TryChangeCapacity(resultingCapacity)) {
            bags[index] = newBag;
            if (OnDataChange != null) OnDataChange.Invoke(index, bags[index]);
            return true;
        }

        return false;
    }
}