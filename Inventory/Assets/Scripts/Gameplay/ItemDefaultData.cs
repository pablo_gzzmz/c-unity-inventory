
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Database/Items/Default")]
public class ItemDefaultData : ItemData {

    [SerializeField]
    private bool isPersistent = false;
    public  override bool IsPersistent { get { return isPersistent; } }

    [SerializeField]
    private bool canStack;
    public  override bool CanStack { get { return canStack; } }
}
