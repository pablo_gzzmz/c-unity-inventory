
using UnityEngine;

[CreateAssetMenu(fileName = "Bag", menuName = "Database/Items/Bag")]
public class BagData : ItemData {

    public override bool CanStack { get { return false; } }
    
    public override bool IsPersistent { get { return true; } }

    [SerializeField]
    private int slots;
    public  int Slots { get { return slots; } }
}
