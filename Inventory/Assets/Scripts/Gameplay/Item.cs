
using UnityEngine;

public class Item : Element<ItemData> {
    
    public override ElementMask Type { get { return ElementMask.Item; } }
    
    public override Color Color { get { return Globals.itemRarityColors[data.Rarity]; } }

    public Item(ItemData _data) : base(_data) { }
}
