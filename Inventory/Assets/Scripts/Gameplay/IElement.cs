
using System;
using UnityEngine;

/// <summary>
/// Elements are any objects that exist as non-3D-world instances, primarily interface-dependant objects like Items.
/// </summary>

public interface IElement {
    
    ElementMask Type { get; }

    long NameID   { get; }
    long UniqueID { get; }

    Sprite Icon          { get; }
    Texture2D CursorIcon { get; }

    Color  Color { get; }
    
    bool CanStack   { get; }
    int  Stacks     { get; set; }
    event Action<int> OnStackChange;
    
    float CooldownFill { get; }
    
    bool Use   ();
}
