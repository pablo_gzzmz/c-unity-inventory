
using System.Collections.Generic;
using UnityEngine;

public static class Database {
    
    private static Dictionary<int, ItemData> items    = new Dictionary<int, ItemData>();

    public static void Initialize() {
        SetItems();
    }

    public static ItemData GetItem(int id) {
        if (items.ContainsKey(id)) return items[id];
        return null;
    }

    private static void SetItems() {
        ItemData[] allItems = Resources.LoadAll<ItemData>("Database/");

        for (int i = 0; i < allItems.Length; i++) {
            items.Add(allItems[i].NameHash, allItems[i]);
        }
    }
}
