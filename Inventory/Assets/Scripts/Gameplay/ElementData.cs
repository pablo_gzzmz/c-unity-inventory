
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ElementData : Dataholder {
    
    public abstract bool CanStack { get; }

    [SerializeField]
    private Texture2D cursorIcon;
    public  Texture2D CursorIcon { get { return cursorIcon; } }

    [SerializeField]
    private float cooldown;
    public  float Cooldown { get { return cooldown; } }

    [SerializeField]
    protected bool isLostAfterUse = false;
    public    bool IsLostAfterUse { get { return isLostAfterUse; } }

    public virtual bool HasUse { get { return false; } }

    protected virtual void OnValidate() {
        if (Icon != null) {
            cursorIcon = Resources.Load<Texture2D>("UI/Cursors/"+Icon.texture.name);
            if (cursorIcon == null) {
                cursorIcon = Resources.Load<Texture2D>("UI/Cursors/Info");
            }
        }
    }
}
