
using System;
using System.Runtime.Serialization;
using UnityEngine;

public static class ElementIDGenerator {

    private static ObjectIDGenerator generator;

    static ElementIDGenerator() {
        generator = new ObjectIDGenerator();
    }

    public static long GetID(object ob, out bool firstTime) {
        return generator.GetId(ob, out firstTime);
    }
}

public abstract class Element<T> :  IElement where T : ElementData {
    
    public abstract ElementMask Type { get; }

    protected T data;
    public    T Data { get { return data; } }

    private long uniqueID;
    public  long UniqueID { get { return uniqueID; } }
    public  long NameID   { get { return data.NameHash; } }

    public Sprite    Icon       { get { return data.Icon; } }
    public Texture2D CursorIcon { get { return data.CursorIcon; } }

    public abstract Color Color { get; }

    public    bool CanStack { get { return data.CanStack; } }
    protected int  stacks = 1;
    public    int  Stacks {
        get { return stacks; }
        set { Debug.Assert(value > 0); stacks = value; if (OnStackChange != null) OnStackChange.Invoke(stacks); } }

    public    event Action<int> OnStackChange;
    
    protected float cooldown;
    public    float Cooldown { get { return cooldown; } }
    protected float totalCooldown;
    public    float TotalCooldown { get { return totalCooldown; } }
    protected float totalCooldownMultiplier;
    public    float CooldownFill  { get { return cooldown * totalCooldownMultiplier; } }
    
    public Element(T _data) {
        data = _data;
        bool firstTime;
        uniqueID = ElementIDGenerator.GetID(this, out firstTime);
    }

    protected void SetCooldown(float cd) {
        totalCooldown = cd;
        totalCooldownMultiplier = 1 / totalCooldown;
        cooldown = totalCooldown * CooldownFill;
    }

    protected virtual void Update() {
        if (cooldown > 0) cooldown -= Time.deltaTime;
    }
    
    public bool Use() {

        return true;
    }
}
