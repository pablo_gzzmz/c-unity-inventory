
using UnityEngine;

public abstract class Dataholder : ScriptableObject {

    [Header("Base data")]
    [Space(6)]
    [SerializeField]
    private Sprite icon;
    public  Sprite Icon { get { return icon; } }

    [Space(10)]
    [Multiline]
    [SerializeField]
    private string description;
    public  string Description { get { return description; } }
    
    private int nameHash;
    public  int NameHash { get { return nameHash; } }

    protected virtual void OnEnable() {
        nameHash = name.GetHashCode();
    }
}
