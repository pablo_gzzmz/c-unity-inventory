
using UnityEngine;

public abstract class ItemData : ElementData {
    
    public  abstract bool IsPersistent { get; } 

    [Space(12)]
    [Header("Item")]
    [Space(6)]
    [SerializeField]
    protected ItemRarity rarity;
    public    ItemRarity Rarity { get { return rarity; } }

    [SerializeField]
    protected bool hasUse = false;
    public override bool HasUse { get { return hasUse; } }

}
