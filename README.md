# README #
#
Author: Pablo González Martínez
#
pablogonzmartinez@gmail.com

Unity3D Engine
C#

This project showcases the implementation of some UI components used for an inventory divided in bags with drag and drop functionality.

# Google Docs doc #
https://docs.google.com/document/d/1APVx-LJnKZViVCzw69izdE7exbh3lAjGApUDz9yWxt8/edit?usp=sharing

# Requirements #
The project has been created with Unity 2017.2.0f3 (64-bit).

# After-download guide: #
It contains no .exe, as the execution of the project itself ignoring the code is not very relevant. It should be opened in Unity using the parent folder containing ‘Assets’ and ‘Project Settings’ folders.

The project is organized in folders. The main scene is labelled as ‘test’, and is contained in the Scenes folder.

All the code is contained in the Scripts folder and has been created exclusively by the author.